local Atlas = {}

function Atlas.new(atlasColumns,atlasRows,mapColumns,mapRows,defaults)
  local instance = {}
  
  instance.columns = {}
  
  for atlasColumn = 1, atlasColumns do
    instance.columns[column] = AtlasColumn.new(atlasColumn,atlasRows, mapColumns, mapRows,defaults)
  end
  
  function instance:width()
    return table.getn(instance.columns)
  end
  
  function instance:height()
    return table.getn(instance.columns[1])
  end
  
  return instance
end

return Atlas
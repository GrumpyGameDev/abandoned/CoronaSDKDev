local Directions = {}

local nexts = {2,3,4,1}
local prevs = {4,1,2,3}
local opposites = {3,4,1,2}
local deltaXs = {0,1,0,-1}
local deltaYs = {-1,0,1,0}
local flags = {1,2,4,8}

function Directions.new()
  local instance = {}
  
  function instance:getNext(direction)
    return nexts[direction]
  end
  
  function instance:getPrevious(direction)
    return prevs[direction]
  end
  
  function instance:getNextX(x,y,direction)
      return x + deltaXs[direction]
  end
  
  function instance:getNextY(x,y,direction)
      return y + deltaYs[direction]
  end

  function instance:getFlag(direction)
    return flags[direction]
  end

  function instance:count()
    return 4
  end
  
  return instance
end

return Directions
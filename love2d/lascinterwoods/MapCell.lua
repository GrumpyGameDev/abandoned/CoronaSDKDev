local MapCell = {}

function MapCell.new(column,row,defaults)
  local instance = {}
  
  instance.column = column
	instance.row=row
  
  for key,value in pairs(defaults) do
    instance[key]=value
  end
  
  return instance
end

return MapCell
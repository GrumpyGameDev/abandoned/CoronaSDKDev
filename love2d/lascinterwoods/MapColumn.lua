local MapColumn = {}
local MapCell = require("MapCell")

function MapColumn.new(column,rows,defaults)
  
  local instance = {}
  
  instance.cells = {}
  
  for row = 1, rows do
    instance.cells[row] = MapCell.new(column, row, defaults)
  end

  return instance
end

return MapColumn
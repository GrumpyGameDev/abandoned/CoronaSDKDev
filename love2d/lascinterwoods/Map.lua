local Map = {}

function Map.new(columns,rows,defaults)
  local instance = {}
  
  instance.columns = {}
  
  for column = 1, columns do
    instance.columns[column] = MapColumn.new(column,rows,defaults)
  end
  
  function instance:width()
    return table.getn(instance.columns)
  end
  
  function instance:height()
    return table.getn(instance.columns[1])
  end
  
  return instance
end

return Map
local Utility = {}

function Utility.randomRange(min,max)
  return math.random(min,max)
end

function Utility.randomArray(array)
  return array[Utility.randomRange(1,table.getn(array))]
end

return Utility
local MazeCell = {}

function MazeCell.new(column,row,directions)
  local instance = {}
  
  instance.directions = directions
  instance.column=column
  instance.row=row
  instance.doors={}
  instance.neighbors={}
  instance.outside = true
  
  function instance:flagify()
    local flags = 0
    for direction = 1, self.directions.count() do
      if self.neighbors[direction]~=nil then
        if self.doors[direction].open
          flags = flags + self.directions.getFlag(direction)
        end
      end
    end
    return flags
  end
  
  return instance
end

return MazeCell
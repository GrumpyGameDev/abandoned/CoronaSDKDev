local AtlasColumn = {}
local Map = require("Map")

function AtlasColumn.net(atlasColumn,atlasRows,mapColumns,mapRows,defaults)
  
  local instance = {}
  
  instance.rows = {}
  
  for atlasRow = 1, atlasRows do
    local map = Map.new(mapColumns, mapRows, defaults)
    map.atlasColumn = atlasColumn
    map.atlasRow = atlasRow
    instance.rows[atlasRow]=map
  end
  
  return instance
end

return AtlasColumn
local TextMessage = {}

local MessageId = require("Message.Id")
local Message = require("Message.Base")

local editId = MessageId.next()
local inputId = MessageId.next()

function TextMessage.editId()
  return editId
end

function TextMessage.inputId()
  return inputId
end

function TextMessage.newEdit(text, start, length)
  local instance = Message.new(TextMessage.editId())
  function instance:getText()
    return text
  end
  function instance:getStart()
    return start
  end
  function instance:getLength()
    return length
  end
  return instance
end

function TextMessage.newInput(text)
  local instance = Message.new(TextMessage.inputId())
  function instance:getText()
    return text
  end
  return instance
end

return TextMessage
local TouchMessage = {}
local MessageId = require("Message.Id")
local Message = require("Message.Base")

local touchMoveMessageId = MessageId.next()
local touchPressMessageId = MessageId.next()
local touchReleaseMessageId = MessageId.next()

function TouchMessage.moveId()
  return touchMoveMessageId
end

function TouchMessage.pressId()
  return touchPressMessageId
end

function TouchMessage.releaseId()
  return touchReleaseMessageId
end

function TouchMessage.newMove(id, x, y, dx, dy, pressure)
  local instance = Message.new(TouchMessage.moveId())
  function instance:getId()
    return id
  end
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  function instance:getDx()
    return dx
  end
  function instance:getDy()
    return dy
  end
  function instance:getPressure()
    return pressure
  end
  return instance
end

function TouchMessage.newPress(id, x, y, dx, dy, pressure)
  local instance = Message.new(TouchMessage.pressId())
  function instance:getId()
    return id
  end
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  function instance:getDx()
    return dx
  end
  function instance:getDy()
    return dy
  end
  function instance:getPressure()
    return pressure
  end
  return instance
end

function TouchMessage.newRelease(id, x, y, dx, dy, pressure)
  local instance = Message.new(TouchMessage.releaseId())
  function instance:getId()
    return id
  end
  function instance:getX()
    return x
  end
  function instance:getY()
    return y
  end
  function instance:getDx()
    return dx
  end
  function instance:getDy()
    return dy
  end
  function instance:getPressure()
    return pressure
  end
  return instance
end

return TouchMessage
local KeyMessage = {}
local Message = require("Message.Base")
local MessageId = require("Message.Id")

local keyReleaseMessageId = MessageId.next()
local keyPressMessageId = MessageId.next()

function KeyMessage.releaseId()
  return keyReleaseMessageId
end

function KeyMessage.pressId()
  return keyPressMessageId
end

function KeyMessage.newRelease(key, scancode)
  local instance = Message.new(KeyMessage.releaseId())
  function instance:getKey()
    return key
  end
  function instance:getScancode()
    return scancode
  end
  return instance
end

function KeyMessage.newPress(key, scancode, isrepeat)
  local instance = Message.new(KeyMessage.pressId())
  function instance:getKey()
    return key
  end
  function instance:getScancode()
    return scancode
  end
  function instance:getRepeat()
    return isrepeat
  end
  return instance
end


return KeyMessage
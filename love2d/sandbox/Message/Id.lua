local MessageId = {}

local nextMessageId = 0

function MessageId.next()
  local result = nextMessageId
  nextMessageId = nextMessageId + 1
  return result
end

return MessageId

local UpdateMessage={}
local MessageId = require("Message.Id")
local Message = require("Message.Base")

local updateId = MessageId.next()

function UpdateMessage.id()
  return updateId
end

function UpdateMessage.new(dt)
  local instance = Message.new(UpdateMessage.id())
  
  function instance:getDt()
    return dt
  end
  
  return instance
end

return UpdateMessage
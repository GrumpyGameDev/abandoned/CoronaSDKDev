local JoystickMessage = {}
local MessageId = require("Message.Id")
local Message = require("Message.Base")

local gamepadAxisId = MessageId.next()
local gamepadPressId = MessageId.next()
local gamepadReleasId = MessageId.next()
local addId = MessageId.next()
local axisId = MessageId.next()
local hatId = MessageId.next()
local pressId = MessageId.next()
local releaseId = MessageId.next()
local removeId = MessageId.next()

function JoystickMessage.gamepadAxisId()
  return gamepadAxisId
end

function JoystickMessage.gamepadPressId()
  return gamepadPressId
end

function JoystickMessage.gamepadReleaseId()
  return gamepadReleaseId
end

function JoystickMessage.addId()
  return addId
end

function JoystickMessage.axisId()
  return axisId
end

function JoystickMessage.hatId()
  return hatId
end

function JoystickMessage.pressId()
  return pressId
end

function JoystickMessage.releaseId()
  return releaseId
end

function JoystickMessage.removeId()
  return removeId
end

function JoystickMessage.newGamepadAxis(joystick, axis, value)
  local instance = Message.new(JoystickMessage.gamepadAxisId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getAxis()
    return axis
  end
  function instance:getValue()
    return value
  end
  return instance
end

function JoystickMessage.newGamepadPress(joystick, button)
  local instance = Message.new(JoystickMessage.gamepadPressId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getButton()
    return button
  end
  return instance
end

function JoystickMessage.newGamepadRelease(joystick, button)
  local instance = Message.new(JoystickMessage.gamepadReleaseId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getButton()
    return button
  end
  return instance
end

function JoystickMessage.newAdd(joystick)
  local instance = Message.new(JoystickMessage.addId())
  function instance:getJoystick()
    return joystick
  end
  return instance
end

function JoystickMessage.newAxis(joystick, axis, value)
  local instance = Message.new(JoystickMessage.axisId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getAxis()
    return axis
  end
  function instance:getValue()
    return value
  end
  return instance
end

function JoystickMessage.newHat(joystick, hat, direction)
  local instance = Message.new(JoystickMessage.hatId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getHat()
    return hat
  end
  function instance:getDirection()
    return direction
  end
  return instance
end

function JoystickMessage.newPress(joystick, button)
  local instance = Message.new(JoystickMessage.pressId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getButton()
    return button
  end
  return instance
end

function JoystickMessage.newRelease(joystick, button)
  local instance = Message.new(JoystickMessage.releaseId())
  function instance:getJoystick()
    return joystick
  end
  function instance:getButton()
    return button
  end
  return instance
end

function JoystickMessage.newRemove(joystick)
  local instance = Message.new(JoystickMessage.removeId())
  function instance:getJoystick()
    return joystick
  end
  return instance
end

return JoystickMessage
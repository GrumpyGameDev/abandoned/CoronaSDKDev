local ResizeMessage = {}

local Message = require("Message.Base")
local MessageId = require("Message.Id")

local resizeMessageId = MessageId.next()

function ResizeMessage.id()
  return resizeMessageId
end

function ResizeMessage.new(width,height)
  local instance = Message.new(ResizeMessage.id())
  
  function instance:getWidth()
    return width
  end
  
  function instance:getHeight()
    return height
  end
  
  return instance
end

return ResizeMessage
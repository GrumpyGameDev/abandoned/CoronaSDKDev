local MessageHandler = {}

function MessageHandler.new(parent)
  
  local instance = {}
  
  local children={}
  
  function instance:getParent()
    return parent
  end
  function instance:setParent(newParent)
    parent = newParent
  end
  function instance:hasParent()
    return parent~=nil
  end
  
  function instance:addChild(child)
    if not self:hasChild(child) then
      table.insert(children,child)
      return true
    else
      return false
    end
  end
  function instance:removeChild(child)
    for index=1,#children do
      if children[index]==child then
        table.remove(children,index)
        return true
      end
    end
    return false
  end
  function instance:hasChild(child)
    for index=1,#children do
      if children[index]==child then
        return true
      end
    end
    return false
  end
  
  function instance:onMessage(message)
      return false
  end
  function instance:handleMessage(message)
    if self:onMessage(message) then
      return true
    else
      if self:hasParent() then
        return self:getParent():handleMessage(message)
      else
        return false
      end
    end
  end
  function instance:broadcast(message,reverse)
    local start = reverse and #children or 1
    local stop = reverse and 1 or #children
    local delta = reverse and -1 or 1
    if not reverse then
      self:onMessage(message)
    end
    for index = start, stop, delta do
      local child = children[index]
      child:broadcast(message,reverse)
    end
    if reverse then
      self:onMessage(message)
    end
  end
  function instance:handleEvent(message,reverse)
    local start = reverse and #children or 1
    local stop = reverse and 1 or #children
    local delta = reverse and -1 or 1
    if not reverse then
      if self:onMessage(message) then
        return true
      end
    end
    for index = start, stop, delta do
      local child = children[index]
      if child:handleEvent(message) then
        return true
      end
    end
    if reverse then
      if self:onMessage(message) then
        return true
      end
    end
  end
  
  return instance
  
end

return MessageHandler
local Bootstrap = require("Bootstrap")
local Application = require(Bootstrap.application())
local ResizeMessage = require("Message.Resize")
local KeyMessage = require("Message.Key")
local MouseMessage = require("Message.Mouse")
local TouchMessage = require("Message.Touch")
local UpdateMessage = require("Message.Update")
local JoystickMessage = require("Message.Joystick")
local TextMessage = require("Message.Text")
local FileMessage = require("Message.File")
local EventHandler = require("Handler.Event")

local application

--Boot
function love.load()
  if (arg[#arg] == "-debug") and (Bootstrap.debug()) then require("mobdebug").start() end
  application = Application.new()
  application:broadcast(EventHandler.initializeMessage())
end

local oldErrhand = love.errhand
function love.errhand(msg)
  oldErrhand(msg)
end

function love.threaderror( thread, errorstr )
end

function love.lowmemory()
end

--focus
function love.focus(focus)
  if focus then
    application:broadcast(EventHandler.gotFocusMessage())
  else
    application:broadcast(EventHandler.lostFocusMessage())
  end
end

function love.mousefocus(focus)
  if focus then
    application:broadcast(EventHandler.gotMouseFocusMessage())
  else
    application:broadcast(EventHandler.lostMouseFocusMessage())
  end
end

--window
function love.quit()
  application:broadcast(EventHandler.quitMessage())
end

function love.resize(w,h)
  application:broadcast(ResizeMessage.new(w,h))
end

function love.visible( visible )
  if visible then
    application:broadcast(EventHandler.showMessage())
  else
    application:broadcast(EventHandler.hideMessage())
  end
end

--keys
function love.keypressed( key, scancode, isrepeat )
  application:handleEvent(KeyMessage.newPress(key,scancode,isrepeat), true)
end

function love.keyreleased( key, scancode )
  application:handleEvent(KeyMessage.newRelease(key,scancode), true)
end

--mouse
function love.mousemoved( x, y, dx, dy, istouch )
  application:handleEvent(MouseMessage.newMove(x, y, dx, dy, istouch), true)
end

function love.mousepressed( x, y, button, istouch )
  application:handleEvent(MouseMessage.newPress(x, y, button, istouch), true)
end

function love.mousereleased( x, y, button, istouch )
  application:handleEvent(MouseMessage.newRelease(x, y, button, istouch), true)
end

function love.wheelmoved( x, y )
  application:handleEvent(MouseMessage.newWheel(x, y), true)
end

--touch
function love.touchmoved( id, x, y, dx, dy, pressure )
  application:handleEvent(TouchMessage.newMove(x, y, dx, dy, pressure), true)
end

function love.touchpressed( id, x, y, dx, dy, pressure )
  application:handleEvent(TouchMessage.newPress(x, y, dx, dy, pressure), true)
end

function love.touchreleased( id, x, y, dx, dy, pressure )
  application:handleEvent(TouchMessage.newRelease(x, y, dx, dy, pressure), true)
end

--Update/Draw
function love.update( dt )
  application:broadcast(UpdateMessage.new(dt))
end

function love.draw()
  Bootstrap.predraw()
  application:broadcast(EventHandler.drawMessage())
  Bootstrap.postdraw()
end

--gamepad/joystick

function love.gamepadaxis( joystick, axis, value )
  application:handleEvent(JoystickMessage.newGamepadAxis(joystick,axis,value), true)
end

function love.gamepadpressed( joystick, button )
  application:handleEvent(JoystickMessage.newGamepadPress(joystick, button), true)
end

function love.gamepadreleased( joystick, button )
  application:handleEvent(JoystickMessage.newGamepadRelease(joystick, button), true)
end

function love.joystickadded( joystick )
  application:handleEvent(JoystickMessage.newAdd(joystick), true)
end

function love.joystickaxis( joystick, axis, value )
  application:handleEvent(JoystickMessage.newAxis(joystick,axis,value), true)
end

function love.joystickhat( joystick, hat, direction )
  application:handleEvent(JoystickMessage.newHat(joystick,hat,direction), true)
end

function love.joystickpressed( joystick, button )
  application:handleEvent(JoystickMessage.newPress(joystick,button), true)
end

function love.joystickreleased( joystick, button )
  application:handleEvent(JoystickMessage.newRelease(joystick,button), true)
end

function love.joystickremoved( joystick )
  application:handleEvent(JoystickMessage.newRemove(joystick), true)
end

--text
function love.textedited( text, start, length )
  application:handleEvent(TextMessage.newEdit(text,start,length))
end

function love.textinput( text )
  application:handleEvent(TextMessage.newInput(text))
end

--Drops
function love.directorydropped(path)
  application:handleEvent(FileMessage.newDirectoryDrop(path))
end

function love.filedropped(file)
  application:handleEvent(FileMessage.newFileDrop(file))
end



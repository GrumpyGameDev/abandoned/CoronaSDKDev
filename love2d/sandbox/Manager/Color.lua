local ColorManager = {}

function ColorManager.new(colorTable)
  local instance = {}
  
  instance.colorTable = colorTable
  
  function instance:getRed(id)
    if self.colorTable[id]~=nil then
      if table.getn(self.colorTable[id])>0 then
        return self.colorTable[id][1]
      else
        return 0
      end
    else
      return 0
    end
  end
  
  function instance:getGreen(id)
    if self.colorTable[id]~=nil then
      if table.getn(self.colorTable[id])>1 then
        return self.colorTable[id][2]
      else
        return 0
      end
    else
      return 0
    end
  end
  
  function instance:getBlue(id)
    if self.colorTable[id]~=nil then
      if table.getn(self.colorTable[id])>2 then
        return self.colorTable[id][3]
      else
        return 0
      end
    else
      return 0
    end
  end
  
  function instance:getAlpha(id)
    if self.colorTable[id]~=nil then
      if table.getn(self.colorTable[id])>3 then
        return self.colorTable[id][4]
      else
        return 255
      end
    else
      return 255
    end
  end
  
  function instance:getRGBA(id)
    return self:getRed(id), self:getGreen(id), self:getBlue(id), self:getAlpha(id)
  end
  
  return instance
end

return ColorManager
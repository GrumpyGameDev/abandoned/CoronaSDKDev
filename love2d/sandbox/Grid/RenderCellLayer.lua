local RenderGridCellLayer = {}

function RenderGridCellLayer.new(imageName,foregroundColor,backgroundColor)
  local instance = {}
  
  function instance:getImageName()
    return imageName
  end
  
  function instance:getForegroundColor()
    return foregroundColor
  end
  
  function instance:getBackgroundColor()
    return backgroundColor
  end
  
  return instance
end

return RenderGridCellLayer
local RenderGridCell = {}

function RenderGridCell.new(column,row)
  local instance = {}
  
  local layers = {}
  
  function instance:getColumn()
    return column
  end
  
  function instance:getRow()
    return row
  end
  
  function instance:getLayers()
    return #layers
  end
  
  function instance:getLayer(layer)
    return layers[layer]
  end
  
  function instance:clear()
    while #layers>0 do
      table.remove(layers)
    end
  end
  
  function instance:add(layer)
    layers[#layers+1]=layer
  end
 
  return instance
end

return RenderGridCell
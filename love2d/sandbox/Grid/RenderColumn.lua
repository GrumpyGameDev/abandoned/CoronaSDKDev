local RenderGridColumn={}
local RenderGridCell = require("Grid.RenderCell")

function RenderGridColumn.new(column,rows)
  local instance = {}
  
  local cells = {}
  
  for row=1,rows do
    cells[#cells+1]=RenderGridCell.new(column,row)
  end
  
  function instance:getColumn()
    return column
  end
  
  function instance:getRows()
    return rows
  end
  
  function instance:getCell(row)
    return cells[row]
  end
  
  return instance
end

return RenderGridColumn
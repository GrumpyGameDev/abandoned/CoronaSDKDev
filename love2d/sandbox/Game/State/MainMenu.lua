local MainMenuState = {}
local EventHandler = require("Handler.Event")
local MenuState = require("Game.State.Menu")

local Menu = require("Menu.Base")
local MenuItem = require("Menu.Item")
local MenuRenderer = require("Renderer.Menu")

local Map = require("Map.Base")
local MapRenderer = require("Renderer.Map")


local Terrains = require("Game.Terrains")
local TerrainManager = require("Manager.Terrain")

function MainMenuState.new(imageManager)
  local menu = Menu.new(1,
    {MenuItem.new("   Start   ","start"),
     MenuItem.new("How To Play","help"),
     MenuItem.new("  Options  ","options"),
     MenuItem.new("   About   ","about"),
     MenuItem.new("   Quit    ","quit")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",40,40,8,8)
  local map = Map.new(20,15)
  local mapRenderer = MapRenderer.new(imageManager,map,0,0,8,8)
  local terrainManager = TerrainManager.new(Terrains)
  
  local terrainInstance = terrainManager:createInstance("grass")
  
  for column=1,map:getColumns() do
    for row=1,map:getRows() do
      map:getCell(column,row):setTerrain(terrainInstance)
    end
  end
  
  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    
    mapRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  function instance:onCommand(command)
      if command=="quit" then
        self:getStateMachine():setState("confirmquit")
        return true
      else
        return false
      end
  end
  
  return instance
end


return MainMenuState
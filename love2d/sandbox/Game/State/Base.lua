local StateBase = {}

local InputFilter = require("Game.InputFilter")
local EventHandler = require("Handler.Event")

function StateBase.new()
  local instance = EventHandler.new(nil,false)
  
  function instance:onFilteredInput(input)
    return false
  end
  
  function instance:onKeyPress(key,scancode,isrepeat)
    local input = InputFilter.fromKeypress(key)
    if input~=nil then
      return instance:onFilteredInput(input)
    else
      return false
    end
  end
  
  function instance:onGamepadPress(joystick,button)
    local input = InputFilter.fromGamepadButton(button)
    if input~=nil then
      return instance:onFilteredInput(input)
    else
      return false
    end
  end
  
  return instance
end

return StateBase
local ConfirmQuitState = {}
local EventHandler = require("Handler.Event")
local Menu = require("Menu.Base")
local MenuItem = require("Menu.Item")
local MenuRenderer = require("Renderer.Menu")
local RenderGrid = require("Grid.Render")
local RenderGridCellLayer = require("Grid.RenderCellLayer")
local GridRenderer = require("Renderer.Grid")
local MenuState = require("Game.State.Menu")

function ConfirmQuitState.new(imageManager)
  local menu = Menu.new(2,
    {MenuItem.new(" Yes ","confirm"),
     MenuItem.new(" No  ","cancel")})
  
  local instance = MenuState.new(menu)
  
  local menuRenderer = MenuRenderer.new(imageManager,menu,"ruby","black",56,56,8,8)
  local renderGrid = RenderGrid.new(20,15)
  local gridRenderer = GridRenderer.new(imageManager,renderGrid,0,0,8,8)
  
  renderGrid:fill(1,1,20,15,"tree","jade","black",true)
  renderGrid:fill(2,2,18,13,"bush","darkjade","black",true)
  renderGrid:writeText(8,7,"Quit?","turquoise","black")
  
  function instance:getImageManager()
    return imageManager
  end
  
  function instance:onDraw()
    
    gridRenderer:render()
    
    menuRenderer:render()
    
    return false
  end
  
  function instance:onCommand(command)
      if command=="confirm" then
        love.event.quit()
        return true
      elseif command=="cancel" then
        self:getStateMachine():setState("mainmenu")
        return true
      else
        return false
      end
  end
  
  return instance
end


return ConfirmQuitState
local MenuState = {}
local EventHandler = require("Handler.Event")
local Menu = require("Menu.Base")
local MenuItem = require("Menu.Item")
local MenuRenderer = require("Renderer.Menu")
local RenderGrid = require("Grid.Render")
local RenderGridCellLayer = require("Grid.RenderCellLayer")
local GridRenderer = require("Renderer.Grid")
local StateBase = require("Game.State.Base")

function MenuState.new(menu)
  local instance = StateBase.new()
  
  function instance:onCommand(command)
    return false
  end
  
  function instance:onFilteredInput(input)
    if input=="up" then
      menu:previousItem()
      return true
    elseif input=="down" then
      menu:nextItem()
      return true
    elseif input=="ok" then
      return self:onCommand(menu:getSelectedItem():getId())
    else
      return false
    end
  end
  
  return instance
end


return MenuState
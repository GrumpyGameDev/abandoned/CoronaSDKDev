local GridState = {}
local EventHandler = require("Handler.Event")
local RenderGrid = require("Grid.Render")
local RenderGridCellLayer = require("Grid.RenderCellLayer")
local GridRenderer = require("Renderer.Grid")

function GridState.new(imageManager)
  local instance = EventHandler.new(nil,true)
  
  local renderGrid = RenderGrid.new(20,15)
  local gridRenderer = GridRenderer.new(imageManager,renderGrid,0,0,8,8)
  
  function instance:getImageManager()
    return imageManager
  end
  
  renderGrid:fill(1,1,15,15,"character32","black","black",true)
  
  renderGrid:getCell(1,1):add(RenderGridCellLayer.new("tagon","carnelian","gold"))
  renderGrid:getCell(1,2):add(RenderGridCellLayer.new("character65","ruby","black"))
  renderGrid:getCell(2,2):add(RenderGridCellLayer.new("character66","ruby","black"))
  renderGrid:getCell(3,2):add(RenderGridCellLayer.new("character67","ruby","black"))
  renderGrid:getCell(4,2):add(RenderGridCellLayer.new("character68","ruby","black"))
  renderGrid:writeText(1,15,"Hello, world!","jade","black",true)
  
  function instance:onDraw()
    
    gridRenderer:render()
    
    return false
  end
  
  function instance:onGamepadRelease(joystick,button)
    if button=="b" then
      self:getStateMachine():setState("menu")
      return true
    else
      return false
    end
  end
  
  
  return instance
end


return GridState
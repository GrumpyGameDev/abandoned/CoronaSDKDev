local GameApplication = {}
local StateHandler = require("Handler.State")
local GameImages = require("Game.Images")
local GamePalette = require("Game.Palette")
local ColorManager = require("Manager.Color")
local ImageManager = require("Manager.Image")
local MainMenuState = require("Game.State.MainMenu")
local ConfirmQuitState = require("Game.State.ConfirmQuit")
local GridState = require("Game.State.Grid")
local InputFilter = require("Game.InputFilter")

local Direction = require("Common.Direction")

function GameApplication.new()
  love.graphics.setDefaultFilter("nearest","nearest",1)--always do this on load
  
  InputFilter.getKeypressMap()["up"]="up"
  InputFilter.getKeypressMap()["down"]="down"
  InputFilter.getKeypressMap()["left"]="left"
  InputFilter.getKeypressMap()["right"]="right"
  InputFilter.getKeypressMap()["x"]="ok"
  InputFilter.getKeypressMap()["z"]="cancel"
  
  InputFilter.getGamepadButtonMap()["dpup"]="up"
  InputFilter.getGamepadButtonMap()["dpdown"]="down"
  InputFilter.getGamepadButtonMap()["dpleft"]="left"
  InputFilter.getGamepadButtonMap()["dpright"]="right"
  InputFilter.getGamepadButtonMap()["a"]="ok"
  InputFilter.getGamepadButtonMap()["b"]="cancel"
  
  local colorManager = ColorManager.new(GamePalette)
  local imageManager = ImageManager.new(GameImages,colorManager)
  
  local instance = StateHandler.new(nil,"mainmenu",{mainmenu=MainMenuState.new(imageManager),confirmquit=ConfirmQuitState.new(imageManager)})
  
  return instance
end


return GameApplication
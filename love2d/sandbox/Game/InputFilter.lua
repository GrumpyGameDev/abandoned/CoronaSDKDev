local InputFilter = {}

local keypressMap = {}
local gamepadButtonMap={}

function InputFilter.getKeypressMap()
  return keypressMap
end

function InputFilter.getGamepadButtonMap()
  return gamepadButtonMap
end

function InputFilter.fromKeypress(key)
  return keypressMap[key]
end

function InputFilter.fromGamepadButton(button)
  return gamepadButtonMap[button]
end

return InputFilter
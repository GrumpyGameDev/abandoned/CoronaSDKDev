return {
  grass={imageName="field",foregroundColor="darkjade",backgroundColor="black"},
  water={imageName="field",foregroundColor="ruby",backgroundColor="darkruby"},
  path={imageName="field",foregroundColor="black",backgroundColor="darksilver"},
  bush={imageName="bush",foregroundColor="darkjade",backgroundColor="black"},
  tree={imageName="tree",foregroundColor="jade",backgroundColor="black"},
  house={imageName="house",foregroundColor="carnelian",backgroundColor="black"}
  }
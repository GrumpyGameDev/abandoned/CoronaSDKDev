  local Direction=require("Common.Direction")
  
  return {
    Direction.new("west","north","south","east",function(x,y) return x-1,y end,8),
    Direction.new("south","west","east","north",function(x,y) return x,y+1 end,4),
    Direction.new("east","south","north","west",function(x,y) return x+1,y end,2),
    Direction.new("north","east","west","south",function(x,y) return x,y-1 end,1)
  }

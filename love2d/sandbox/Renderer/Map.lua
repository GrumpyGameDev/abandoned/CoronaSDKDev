local MapRenderer = {}

function MapRenderer.new(imageManager,map,offsetX,offsetY,columnWidth,rowHeight)
  local instance = {}
  
  function instance:render()
    for row=1,map:getRows() do
      for column=1,map:getColumns() do
        local x, y = column*columnWidth-columnWidth+offsetX,row*rowHeight-rowHeight+offsetY
        local cell = map:getCell(column,row)
        
        local terrain = cell:getTerrain()
        if terrain~= nil then
          local image = imageManager:getImage(terrain:terrain():imageName(),terrain:terrain():foregroundColor(),terrain:terrain():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
        local creature = cell:getCreature()
        if creature~= nil then
          local image = imageManager:getImage(creature:creature():imageName(),creature:creature():foregroundColor(),creature:creature():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
        local effect = cell:getEffect()
        if effect~= nil then
          local image = imageManager:getImage(effect:effect():imageName(),effect:effect():foregroundColor(),effect:effect():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
        local item = cell:getItem()
        if item~= nil then
          local image = imageManager:getImage(item:item():imageName(),item:item():foregroundColor(),item:item():backgroundColor())
          love.graphics.draw(image,x,y)
        end
        
      end
    end
  end
  
  return instance
end

return MapRenderer
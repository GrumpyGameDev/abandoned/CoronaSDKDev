local GridRenderer = {}

function GridRenderer.new(imageManager,renderGrid,offsetX,offsetY,columnWidth,rowHeight)
  local instance = {}
  
  function instance:render()
    for row=1,renderGrid:getRows() do
      for column=1,renderGrid:getColumns() do
        for layer=1,renderGrid:getCell(column,row):getLayers() do
          local cellLayer = renderGrid:getCell(column,row):getLayer(layer)
          love.graphics.draw(imageManager:getImage(cellLayer:getImageName(),
              cellLayer:getForegroundColor(),cellLayer:getBackgroundColor()),column*columnWidth-columnWidth+offsetX,row*rowHeight-rowHeight+offsetY)
        end
      end
    end
    
  end
  
  return instance
end

return GridRenderer
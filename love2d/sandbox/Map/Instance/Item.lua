local ItemInstance = {}

function ItemInstance.new(item)
  local instance = {}
  if item.instance~=nil then
    local descriptor = item.instance()
    for k,v in pairs(descriptor) do
      if type(v)=="function" then
        instance[k]=v
      else
        instance[k]=function(a) return v end
      end
    end
  end
  function instance:item()
    return item
  end
  return instance
end

return ItemInstance
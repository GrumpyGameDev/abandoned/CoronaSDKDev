local TerrainInstance = {}

function TerrainInstance.new(terrain,data)
  local instance = {}
  if terrain.instance~=nil then
    local descriptor = terrain.instance(data)
    for k,v in pairs(descriptor) do
      if type(v)=="function" then
        instance[k]=v
      else
        instance[k]=function(a) return v end
      end
    end
  end
  function instance:terrain()
    return terrain
  end
  return instance
end

return TerrainInstance
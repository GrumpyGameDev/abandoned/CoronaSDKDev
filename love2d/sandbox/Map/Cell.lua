local Cell = {}

function Cell.new(mapColumns,row,terrain,item,creature,effect)
  local instance = {}
  
  function instance:getRow()
    return row
  end
  
  function instance:getColumn()
    return mapColumns:getColumn()
  end
  
  function instance:getMap()
    return mapColumns:getMap()
  end
  
  function instance:getTerrain()
    return terrain
  end
  
  function instance:setTerrain(newTerrain)
    terrain = newTerrain
  end
  
  function instance:getItem()
    return item
  end
  
  function instance:setItem(newItem)
    item = newItem
  end
  
  function instance:getCreature()
    return creature
  end
  
  function instance:setCreature(newCreature)
    creature = newCreature
  end
  
  function instance:getEffect()
    return effect
  end
  
  function instance:setEffect(newEffect)
    effect = newEffect
  end
  
  return instance
end

return Cell
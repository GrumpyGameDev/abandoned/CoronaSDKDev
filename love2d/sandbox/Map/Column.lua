local Column={}
local MapCell = require("Map.Cell")

function Column.new(map,column,rows)
  local instance={}
  
  local cells = {}
  
  function instance:getMap()
    return map
  end
  
  function instance:getColumn()
    return column
  end
  
  for row=1,rows do
    cells[row] = MapCell.new(instance,row)
  end
  
  function instance:getCell(row)
    return cells[row]
  end
  
  return instance
end

return Column
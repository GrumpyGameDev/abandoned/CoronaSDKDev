local Menu = {}

function Menu.new(selectedIndex,items)
  local instance = {}
  
  function instance:getSelectedIndex()
    return selectedIndex
  end
  
  function instance:getItems()
    return #items
  end
  
  function instance:getItem(index)
    return items[index]
  end
  
  function instance:getSelectedItem()
    return self:getItem(self:getSelectedIndex())
  end
  
  function instance:nextItem()
    if selectedIndex==self:getItems() then
      selectedIndex = 1
    else
      selectedIndex = selectedIndex + 1
    end
  end
  
  function instance:previousItem()
    if selectedIndex==1 then
      selectedIndex = self:getItems()
    else
      selectedIndex = selectedIndex - 1
    end
  end
  
  return instance
end

return Menu
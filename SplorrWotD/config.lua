-- config.lua

application =
{
	content =
	{
		width = 360,
		height = 640,
		scale = "letterbox",
		xAlign = "center",
		yAlign = "center"
	},
}